output "new_account_id" {
  description = "This is the new account ID"
  value       = aws_organizations_account.new_account.id
}

output "new_account_arn" {
  description = "This is the new account ARN"
  value       = aws_organizations_account.new_account.arn
}
