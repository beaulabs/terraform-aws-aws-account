variable "new_aws_account_name" {
  description = "New AWS account name to be created"
}

variable "new_aws_account_name_email" {
  description = "Email associated with the new account name to be created"
}
